#!/bin/bash

#NAGIOS RETURN CODES
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

#DEFAULT
WARNING_THRESHOLD=${WARNING_THRESHOLD:="30"}
CRITICAL_THRESHOLD=${CRITICAL_THRESHOLD:="100"}


function usage()
{
cat << EOF
usage: $0 [options]

This script checks AWS ECS Instance Status.

We assume that the binary JQ is installed. Also we assume that the AWS CLI binary is installed and that the
credentials are set up for the user who is executing this script.

OPTIONS:
    -h or --help           Show this message

    -v or --verbose        Optional: Show verbose output

    --instanceId=x         Required: The AWS EC2 Instance Id

    --region=x             Required: The region the AWS EC2 Instance is located in. Example: "us-east-1" 
                           

EOF
}

function error()
{
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    echo -e "${RED}${1}${NC}";
}
function verbose
{
    if [[ ${VERBOSE} -eq 1 ]];
    then
        echo $1;
    fi
}

# Check Params
if [ $# -eq 0 ]
then
    usage;
    exit ${STATE_UNKNOWN};
fi

INSTANCE_ID=""
REGION=""

for i in "$@"
do
case ${i} in
    --instanceid=* )
        INSTANCE_ID="${i#*=}"
        shift ;
        ;;
    --region=* )
        REGION="${i#*=}"
        shift ;
        ;;
    -v | --verbose )
        VERBOSE=1
        shift ;
        ;;
    help | --help | -h)
        usage ;
        exit ${STATE_UNKOWN};
        ;;
    *)
        error "Error, unkown parameter - \"${i}\"";
        echo ""
            usage ;
            exit ${STATE_UNKNOWN};
            ;;
    esac
done


#Validate Input
if [[ "$INSTANCE_ID" == "" ]]; then
    echo "Missing AWS INSTANCE-ID" 1>&2
    exit $STATE_UNKNOWN
fi

INSTANCE_STATE=$(aws ec2 describe-instances --instance-id $INSTANCE_ID --region $REGION --output text --query 'Reservations[*].Instances[*].State.Name');
ERROR_CODE=$?
if [ $ERROR_CODE != 0 ]; then
    echo "Error code: ($ERROR_CODE) - Please refer to https://docs.aws.amazon.com/apigateway/api-reference/handling-errors/" 
    exit $STATE_UNKNOWN
fi

INSTANCE_NAME=$(aws ec2 describe-tags --filters Name=resource-id,Values=$INSTANCE_ID Name=key,Values=Name --query Tags[].Value --output text);
ERROR_CODE=$?

if [ $ERROR_CODE != 0 ]; then
    echo "Error code: ($ERROR_CODE) - Please refer to https://docs.aws.amazon.com/apigateway/api-reference/handling-errors/" 
    exit $STATE_UNKNOWN
fi

#Check state


if [ "$INSTANCE_STATE" != "running" ]; then
    echo "EC2 Instance $INSTANCE_NAME (id=$INSTANCE_ID) is $INSTANCE_STATE."
    exit $STATE_CRITICAL
else
    #Running
    echo "EC2 Instance $INSTANCE_NAME (id=$INSTANCE_ID) is $INSTANCE_STATE."
    exit $STATE_OK
fi
